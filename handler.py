import json
import pprint
from typing import Dict, List, Set
import sys


class Element:
    def __init__(self, idn: int, height: float):
        self.id = idn
        self.height = height
        self.neighbors = set()

    def add_neighbors(self, neighbors: Set):
        self.neighbors = self.neighbors.union(neighbors)


class Solver:
    def __init__(self):
        self.elements = {}

    def solve(self, n: int, data: Dict) -> List:
        self._create_elements(data["values"])
        self._map_neighbors(data["elements"])
        return self._find_local_maxima(n)

    def _find_local_maxima(self, n=1):
        sorted_heights = sorted(
            self.elements.items(), key=lambda x: x[1].height, reverse=True
        )
        skip = set()
        local_maxima = 0
        response = []
        for idn, height in sorted_heights:
            if idn in skip:
                continue

            peak = True
            for neigh in self.elements[idn].neighbors:
                neigh_height = self.elements[neigh].height
                if self.elements[idn].height <= neigh_height:
                    peak = False
                    break
                # This element can not be a local maxima
                if neigh not in skip:
                    skip.add(neigh)
            if peak:
                local_maxima += 1
                response.append({"element_id": idn, "value": self.elements[idn].height})
                if local_maxima == n:
                    break

        return response

    def _create_elements(self, values: List):
        for value in values:
            idn = value["element_id"]
            height = value["value"]
            self.elements[idn] = Element(idn, height)

    def _map_neighbors(self, element_data: List):
        node_shared_elements = {}
        # Map nodes to their elements
        # This will give us noisy information about neighbors
        for elem in element_data:
            idn = elem["id"]
            nodes = elem["nodes"]
            for node in nodes:
                if _ := node_shared_elements.get(node) is None:
                    node_shared_elements[node] = []
                # We do not care about repeated elements here
                # They will be filtered next
                node_shared_elements[node].append(idn)

        # Map elements to their respective neighbors eliminating repetitions
        for _, neighbor_list in node_shared_elements.items():
            for neigh in neighbor_list:
                neighbors = set([n for n in neighbor_list if n != neigh])
                self.elements[neigh].add_neighbors(neighbors)


def view_spot_finder(event, context):
    # The idea here is to emulate the behavior of the
    # offline input. In an actual serverless app we would probably
    # get the mesh_file from the endpoint hosting the file

    try:
        fname = event["mesh_file"]
        view_spots = event["view_spots"]
    except KeyError:
        return {
            "Error": (
                'The input is not valid, please pass {"mesh_file": '
                '<file_name>, "view_spots": <number of view spots>'
            )
        }

    with open(fname, "r") as f:
        event = json.load(f)
    solver = Solver()
    return solver.solve(view_spots, event)


if __name__ == "__main__":
    fname = sys.argv[1]
    number_spots = int(sys.argv[2])
    with open(fname, "r") as f:
        data = json.load(f)

    solver = Solver()
    pprint.pprint(solver.solve(number_spots, data))
