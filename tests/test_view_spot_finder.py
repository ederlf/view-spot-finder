from handler import Solver
import pytest


@pytest.fixture
def element_data():
    elements = [i for i in range(1, 5)]
    element_data = []
    for idx, elem in enumerate(elements):
        element = {}
        element["id"] = elem
        element["nodes"] = [i for i in range(idx + 1, idx + 4)]
        element_data.append(element)
    return element_data


@pytest.fixture
def solver(element_data):
    solver = Solver()
    values = []
    heights = [1, 2, 5, 4]
    for i, height in zip(range(1, 5), heights):
        value = {}
        value["element_id"] = i
        value["value"] = height
        values.append(value)
    solver._create_elements(values)
    solver._map_neighbors(element_data)
    return solver


@pytest.fixture
def neighbors():
    return {1: [2, 3], 2: [1, 3, 4], 3: [1, 2, 4], 4: [2, 3]}


def test_map_neighbors(solver, neighbors):
    for elem in neighbors:
        assert solver.elements[elem].neighbors == set(neighbors[elem])


def test_find_local_maxima(solver):
    response = solver._find_local_maxima().pop()
    assert response["element_id"] == 3
    assert response["value"] == 5
