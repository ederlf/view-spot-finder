# View spot finder

The code in this repository solves the local maxima problem for a 2D triangle mesh.

The repository and code structure follows the [Serverless framework](https://www.serverless.com).
Therefore the execution can be either local, via the direct invocation of the Python 3 terminal or the Serverless
framework `sls invoke local` command, or deployed on AWS. 

## Running the code

The local execution of the code requires only Python >= 3.8. 

```
python3 handler.py <mesh_file> <view_spot_number>
```

Running with the `sls` command naturally requires installing the Serveless framework.
The instruction are dependent on the Operating System so please refer to the [getting started](https://www.serverless.com/framework/docs/getting-started) 
page.

After installing the `sls` command run:

```
serverless invoke local --function view_spot_finder --data '{"mesh_file": <mesh_file>, "view_spots": <view_spot_number>}'
```

The idea of the data parameters was to keep it similar to running it as a Python program. You must pass the whole path
of the `mesh_file` if it is not on the working directory of the `sls` command execution.

We could have of course have passed the file contents to the request, so there would not the need to think about
the file location. However the idea here was to think the file would be retrieved by the name from another endpoint
during the execution of the serverless function.

## The solution

The solution consists on initially organizing the data so we could
easily retrieve the heights and neighbors of the elements. Thus we define
a class `Element` that will contain the necessary information to solve the problem.

The Element objects are created and initialized with the id and height data from the `values`
in the data set. These objects are stored in a dictionary with the Element id as the key.

The neighbors from each Element are obtained by first mapping elements that share the same node.
This step goes through the `elements` data adding the element id to a list of elements. Such a list 
is the value of a dictionary that maps nodes to their respective elements.

With the list of neighbors it is easy to infer the neighborhood information. There is only to
map the Elements to neighbors and to filter any redundant id from the previous step.

With the neighborhood information we can then finally look for the local maxima.
Since one of the pre-requisites is to return the local maxima from the heighest to the smallest we
create a list of sorted Elements per height.

For each element in the list we check if the neighbors are higher than the current element.
If a neighbor is higher we move to the next Element in the list. Otherwise, if the neighbor
is smaller it will never be a local maxima, thus we add that neighbor id to a set of Elements
that should be skipped. This optimization is the key for the fast execution of the solution.

When we find a local maxima, i.e., all the neighbors are smaller, we add it to a response dictionary.
The process goes until the all elements are processed or until the specified maximum number of local maxima 
is reached.












